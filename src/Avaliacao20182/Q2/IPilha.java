package Avaliacao20182.Q2;

public interface IPilha {
    void push(Object dado) throws Exception;
    Object pop() throws Exception;
    Object topo() throws Exception;
    boolean estahVazia();
}
